---
layout: 2021/post
section: proposals
category: talks
author: Victor Suarez
title: Desarrollo de Juegos Retro con herramientas Libres
---

Muchos recordaran los tiempos de las máquinas de 8 y 16 bits. Muchos hemos crecido con ellas y ahora nos preguntamos... ¿Sera posible de hacer algo para aquellas consolas u ordenadores que tanto nos dieron? Y aun mejor; ¿Con herramientas libres? Esto y mucho más lo explicaremos en esta charla donde se mostrarán algunos proyectos libres de juegos y herramientas para desarrollarlos también libres.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

En esta charla se mostrarán como podemos hoy en día con las herramientas libres que disponemos, como poder desarrollar para plataformas de 8 y 16 bits. Desde Spectrum, MSX o atary ST sin olvidar de las consolas como NES, Game Boy o la mítica Mega Drive. Hoy en día podemos encontrar juegos, herramientas y frameworks para poder desarrollar para estas plataformas.

Si eres un nostalgico de lo retro, esta es tu charla donde podras recordar viejos tiempos y ver como hoy en día aunque han pasado más de 30 años de algunas de estas plataformas, siguen estando vigentes y se sigue desarrollando para ellas.

## Público objetivo

Cualquier persona interesada en el mundo del desarrollo retro y el homebrew para dispositivos retro de 8 y 16 bits.

## Ponente(s)

Ingeniero en informática; que siempre esta tratando de aprender cosas nuevas. Además de que le encanta el desarrollo retro y esta siempre tratando de organizar y dar charlas para que los demás aprendan.

### Contacto(s)

-   Nombre: Víctor Suárez García
-   Email: zerasul@gmail.com
-   Web personal: https://zerasul.me
-   Mastodon (u otras redes sociales libres): 
-   Twitter: https://twitter.com/zerasul
-   Gitlab: 
-   Portfolio o GitHub (u otros sitios de código colaborativo): https://github.com/zerasul

## Comentarios



## Preferencias de privacidad

(Si quieres que tu información de contacto sea anónima, mándamos las propuesta mediante los formularios de la web: <https://eslib.re/2021/propuestas/charlas/>)

-   []  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
