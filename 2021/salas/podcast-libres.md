---
layout: 2021/post
section: proposals
category: devrooms
title: Podcast Libres
---

## Descripción de la sala

Nos gustaría colaborar con la eslibre este año, creando un puente entre el podcasting y los creadores de contenido de cultura libre y software libre en youtube con el evento de Eslibre. Para poder sumar a la difusión del evento en podcast y canales de youtube y aumentar la contribución a otras comunidades al Eslibre.
Nuestra idea es la de crear una sala en la que se emitan grabaciones en diferido de podcast de diferentes referentes de estas comunidades y hacer grabaciones antes y después del evento que mejoren su difusión en estas comunidades. 

Según vayamos cerrando colaboraciones en estos dos ámbitos los iremos comunicando a la organización de Eslibre. 
De momento estamos Jorge Lama y David Vaquero coordinando esta posibilidad y contactando con posibles creadores de contenido que puedan aportar a esta iniciativa. 

Para poder dar visibilidad previa o después del evento estaría guay saber con quien de la organización podríamos contar para hacer las grabaciones de la promoción del evento Eslibre. 

## Comunidad que la propone

#### Jorge Lama y David Vaquero

Ambos colaboramos activamente en la comunidad de cultura libre y software libre, lo hacemos a nivel personal. 

-   Web de la comunidad: <http://cursosdedesarrollo.com>
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: David Vaquero
-   Email de contacto: pepesan@gmail.com

## Público objetivo

Todo aquel interesado en la organización y promoción de un evento de software y cultura libres. 

## Formato

Podcast grabado y emitido en diferido en una sala a parte en el evento.

## Preferencia horaria

-   Duración: 60 minutos de media por cada podcast
-   Día: Según necesidad

## Comentarios



## Preferencias de privacidad

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
